#!/usr/bin/env bash
# See VERSIONING.md for an explanation.

set -e
RELEASE_VERSION="$1"
NEXT_DEV_VERSION="$2"

if [ -z "$RELEASE_VERSION" ]; then
    echo "Usage: $0 release-version [next-dev-version]" >&2
    exit 1
fi

if [ "${KERNEL_NAME}" == "Darwin" ]; then
  SCRIPT=$(realpath $0)
else
  SCRIPT=$(readlink -f $0)
fi
SCRIPTPATH=`dirname $SCRIPT`
CURRENT_VERSION=`PYTHONPATH=${SCRIPTPATH} python3 -m benchmark.version`

if [ -z "$NEXT_DEV_VERSION" ]; then
    NEXT_DEV_VERSION=`PYTHONPATH=${SCRIPTPATH} python3 <<EOT
import benchmark.version
print(benchmark.version.next_dev_version('${RELEASE_VERSION}'))
EOT`
else
    if [[ "$NEXT_DEV_VERSION" != *.dev0 ]]; then
        echo "The next development version MUST end in .dev0, e.g. '1.0.dev0'" >&2
        exit 2
    fi
fi

RELEASE_BRANCH="release-$RELEASE_VERSION"

echo "Current version    : $CURRENT_VERSION"
echo "To be released     : $RELEASE_VERSION in branch $RELEASE_BRANCH"
echo "Next master version: $NEXT_DEV_VERSION"

GIT=`which git`
if [ -z "${GIT}" ]; then
  echo "ERROR: Git is not found, can not continue."
  exit 1
fi

GIT_C="${GIT} -C ${SCRIPTPATH}"
if [ $(${GIT_C} rev-parse --abbrev-ref HEAD) != "master" ]; then
    echo "You are NOT on the master branch, refusing to run." >&2
    exit 3
fi

echo
echo "Press [ENTER] to create version commits and the release branch."
if [ -z "$2" ]; then
    echo "If you don't like the auto-generated next dev version, pass it on the CLI."
fi
read dummy

echo "Creating branch $RELEASE_BRANCH"
${GIT_C} checkout master -b ${RELEASE_BRANCH}
sed -i'' "s/^version = '.*'/version = '$RELEASE_VERSION'/" benchmark/version.py
${GIT_C} commit -m "Bumped version to $RELEASE_VERSION" benchmark/version.py

echo "Bumping version to $NEXT_DEV_VERSION in master branch"
${GIT_C} checkout master
sed -i'' "s/^version = '.*'/version = '$NEXT_DEV_VERSION'/" benchmark/version.py
${GIT_C} commit -m "Bumped version to $NEXT_DEV_VERSION" benchmark/version.py

echo "Checking out branch $RELEASE_BRANCH for bundling"
${GIT_C} checkout ${RELEASE_BRANCH}

echo "Done. Please investigate and push both master and $RELEASE_BRANCH branches:"
echo
echo "git push origin $RELEASE_BRANCH"
echo "git co master && git push origin master"
