# Blender Benchmark Client

Run benchmarks, collect data!

## CLI usage

* Ensure python3 is installed
* Checkout the Blender scenes from 
https://svn.blender.org/svnroot/bf-blender/trunk/lib/benchmarks/cycles/ 
into a `scenes` directory
* Copy the `config/farm.cfg.example` to `config/farm.cfg` and edit if needed. If device_type is CPU, device_name will be ignored.
* Run `benchmark/farm.py`
* The results will be stored in the output_dir specified in `config/farm.cfg`


## Build for release

### macOS

* Open Xcode, view account settings and ensure the presence of a macOS distribution certificate 
of type "Developer ID Application".
* Open Terminal, run `security find-identity` and ensure the presence of valid identity
* Set env variable CODESIGN_IDENTITY to the valid identity
* Set env variable STORAGE_DIRECTORY to a directory where the bundling will happen
* Run `bundle/bundle.sh`

The script will generate a .zip file containing the signed .dmg
