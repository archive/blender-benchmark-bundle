# Blender Benchmark Client versioning guideline

The version of the Benchmark Client is stored in `benchmark/version.py`.
The version number MUST adhere to [PEP 440](https://www.python.org/dev/peps/pep-0440/).

## TL;DR

To build a release branch for version `1.3`, run:

    ./create_version_branch.sh 1.3

This will create a branch `release-1.3`, which will be checked out in your
working directory once it's done. Furthermore, it'll bump the version in the
`master` branch to `1.4.dev0`. If you release a beta release, it'll bump the
beta number instead of the minor number.


## The long text

Since we make it as easy as possible to submit benchmark data, it is
conceivable that test data is submitted accidentally during development.
To reduce the impact of such a mistake, the version should end in `.devN`
(where `N` is a nonnegative integer) for all in-development versions.
Only versions that are actually released should drop this suffix. A typical
development flow for release `1.3` would thus be:

- Set version to `1.3.dev0` and commit in Git.
- Do more development until 1.3 is ready to be released.
- Create a branch `release-1.3` for the release.
- On `master` branch: set version to `1.4.dev0` and commit.
- On release branch: set version to `1.3` and commit.
- Tag this commit as `v1.3`.
- Build the release files based on the `v1.3` tag or `release-1.3` branch.
- Use the release branch for fixes etc.

This way the `master` branch always has development versions.
