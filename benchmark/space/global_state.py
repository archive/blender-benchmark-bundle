import enum
import threading
import typing


class G:
    """Global state of the Benchmark Client."""

    class State(enum.Enum):
        welcome = 1
        running = 2
        complete = 3
        submitting = 4

    state = State.welcome
    result_platform = ''
    progress_status = ''
    result_stats = ''
    result_dict = {}
    background_image_path = ""
    scene_status = {}
    cancel = False

    cached_system_info = {}
    cached_compute_devices = []
    results_submitted = False
    results_url = ''

    images = {}
    current_progress = 0.0
    progress_lock = threading.Lock()

    submission_exception: typing.Optional[Exception] = None

    @classmethod
    def reset(cls):
        """Reset the global state."""
        cls.state = G.State.welcome
        cls.result_platform = ''
        cls.progress_status = ''
        cls.result_stats = ''
        cls.result_dict = {}
        cls.background_image_path = ""
        cls.scene_status = {}
        cls.results_submitted = False
        cls.results_url = ''
        cls.submission_exception = None
