class COLORS_DUMMY:
    HEADER = ''
    OKBLUE = ''
    OKGREEN = ''
    WARNING = ''
    FAIL = ''
    ENDC = ''
    BOLD = ''
    UNDERLINE = ''


class COLORS_ANSI:
    HEADER = '\033[94m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


VERBOSE = False
COLORS = COLORS_DUMMY


class DefaultLoggerProvider:
    def HEADER(self, *args):
        print(COLORS.HEADER + COLORS.BOLD, end="")
        print(*args, end="")
        print(COLORS.ENDC)

    def WARNING(self, *args):
        print(COLORS.WARNING + COLORS.BOLD, end="")
        print(*args, end="")
        print(COLORS.ENDC)

    def ERROR(self, *args):
        print(COLORS.FAIL + COLORS.BOLD, end="")
        print(*args, end="")
        print(COLORS.ENDC)

    def OK(self, *args):
        print(COLORS.OKGREEN + COLORS.BOLD, end="")
        print(*args, end="")
        print(COLORS.ENDC)

    def BOLD(self, *args):
        print(COLORS.BOLD, end="")
        print(*args, end="")
        print(COLORS.ENDC)

    def INFO(self, *args):
        print(*args)

    def DEBUG(self, *args):
        # TODO(sergey): Add check that debug is enabled.
        if False:
            print(*args)

    def FATAL(self, *args):
        import sys
        ERROR(*args)
        sys.exit(1)


LOGGER_PROVIDER = DefaultLoggerProvider()


def HEADER(*args):
    LOGGER_PROVIDER.HEADER(*args)


def WARNING(*args):
    LOGGER_PROVIDER.WARNING(*args)


def ERROR(*args):
    LOGGER_PROVIDER.ERROR(*args)


def OK(*args):
    LOGGER_PROVIDER.OK(*args)


def BOLD(*args):
    LOGGER_PROVIDER.BOLD(*args)


def INFO(*args):
    LOGGER_PROVIDER.INFO(*args)


def DEBUG(*args):
    LOGGER_PROVIDER.DEBUG(*args)


def FATAL(*args):
    LOGGER_PROVIDER.FATAL(*args)


def supportsColor():
    """
    Returns True if the running system's terminal supports color, and False
    otherwise.
    """

    import sys
    import os

    plat = sys.platform
    supported_platform = plat != 'Pocket PC' and (plat != 'win32' or
                                                  'ANSICON' in os.environ)
    # isatty is not always implemented, #6223.
    is_a_tty = hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()
    if not supported_platform or not is_a_tty:
        return False
    return True


def init():
    if not VERBOSE and supportsColor():
        global COLORS
        COLORS = COLORS_ANSI


def setProvider(provider):
    global LOGGER_PROVIDER
    LOGGER_PROVIDER = provider


def restoreDefaultProvider():
    global LOGGER_PROVIDER
    LOGGER_PROVIDER = DefaultLoggerProvider()
