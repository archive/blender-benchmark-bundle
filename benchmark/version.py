"""Version info for the Benchmark Client.

Run with `python3 -m benchmark.version` to print the version number.
"""

import functools

# This version number MUST adhere to PEP 440.
# https://www.python.org/dev/peps/pep-0440/
#
# It is sent to the MyData server in the 'User Agent' HTTP header, and
# it's also included in error reports.
version = '1.0b3.dev0'


@functools.lru_cache(maxsize=1)
def formatted(the_version=version) -> str:
    """Format the version for showing on the splash screen.

    >>> formatted('1.0')
    '1.0'
    >>> formatted('1.0b2')
    '1.0 Beta 2'
    >>> formatted('1.0rc3')
    '1.0 Release Candidate 3'
    >>> formatted('1.0b2.dev0')
    '1.0 Beta 2 (development)'
    >>> formatted('1.0.dev0')
    '1.0 (development)'
    >>> formatted('1.0a0.dev0+e0e752c')
    '1.0 Alpha (development e0e752c)'
    """

    from benchmark.foundation.third_party.packaging.version import parse

    parsed_version = parse(the_version)
    inner_version = parsed_version.version

    parts = [parsed_version.base_version]

    if inner_version.pre:
        a_b_rc, num = inner_version.pre
        name = {'a': 'Alpha',
                'b': 'Beta',
                'rc': 'Release Candidate'}[a_b_rc]
        if num:
            parts.append(f'{name} {num}')
        else:
            parts.append(name)

    local_ver = parsed_version.local
    if local_ver:
        parts.append(f'(development {local_ver})')
    elif inner_version.dev:
        parts.append(f'(development)')

    return ' '.join(parts)


def next_dev_version(the_version=version) -> str:
    """Returns the version string of the next development version.

    If an alpha/beta/RC version was given, that number is increased.
    Otherwise the minor revision is increased.

    >>> next_dev_version('1')
    '1.1.dev0'
    >>> next_dev_version('1.0')
    '1.1.dev0'
    >>> next_dev_version('1.0b2')
    '1.0b3.dev0'
    >>> next_dev_version('1.0rc3')
    '1.0rc4.dev0'
    >>> next_dev_version('2.51.dev0')
    '2.52.dev0'
    >>> next_dev_version('2.51.dev0+localstuff')
    '2.52.dev0'
    """

    from benchmark.foundation.third_party.packaging.version import parse

    parsed_version = parse(the_version)
    inner_version = parsed_version.version

    # Increase the alpha/beta/RC number
    if inner_version.pre:
        a_b_rc, num = inner_version.pre
        return f'{parsed_version.base_version}{a_b_rc}{num+1}.dev0'

    # Increase the minor release
    major = inner_version.release[0]
    if len(inner_version.release) == 1:
        return f'{major}.1.dev0'

    minor = inner_version.release[1] + 1
    return f'{major}.{minor}.dev0'


if __name__ == '__main__':
    import doctest

    doctest.testmod()
    print(version)
