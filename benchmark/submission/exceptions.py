import requests


class CommunicationError(requests.exceptions.BaseHTTPError):
    """Raised when we get an invalid status code form the MyData server."""

    def __init__(self, message: str, response: requests.Response):
        self.message = message
        self.status_code = response.status_code
        self.body = response.text

        if response.headers.get('Content-Type', '') == 'application/json':
            self.json = response.json()
        else:
            self.json = None

    def __str__(self):
        return f'{self.message}; ' \
               f'status_code={self.status_code}; json={self.json}; body={self.body}'


class TokenTimeoutError(Exception):
    """Raised when there was a timeout waiting for a client token."""
