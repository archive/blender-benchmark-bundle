import logging
import threading

from ..space import G
from . import exceptions

log = logging.getLogger(__name__)


def submit_benchmark_bgthread(benchmark_data: dict) -> threading.Thread:
    """Submit benchmark data in a background thread.

    This will update G.xxx to reflect the state of the submission.
    """

    thread = threading.Thread(target=_submit_and_update_g, args=(benchmark_data,))
    thread.start()
    return thread


def _submit_and_update_g(benchmark_data: dict) -> None:
    with G.progress_lock:
        G.submission_exception = None
        G.state = G.State.submitting

    def set_exception(exception):
        with G.progress_lock:
            G.state = G.State.complete
            G.submission_exception = exception

    try:
        submit_benchmark(benchmark_data)

    except exceptions.CommunicationError as ex:
        log.error('Error %d submitting benchmark: %s', ex.status_code, ex.message)
        if ex.json:
            log.error('Response JSON: %r', ex.json)
        else:
            log.error('Response body: %r', ex.body)
        set_exception(ex)
        return

    except exceptions.TokenTimeoutError as ex:
        log.warning('Timeout waiting for a client token. Just try submitting again.')
        set_exception(ex)
        return

    except Exception as ex:
        log.error('error submitting benchmark: %s', ex)
        set_exception(ex)
        return

    with G.progress_lock:
        G.state = G.State.complete
        G.results_submitted = True


def submit_benchmark(benchmark_data: dict):
    """Submit benchmark data to MyData.

    Authenticates the user via the web browser and Blender ID if necessary.
    Authentication tokens are stored on disk and validated before reusing.
    """

    import logging
    import os

    from .client import BenchmarkClient

    mydata_url = os.environ.get('MYDATA') or 'https://mydata.blender.org/'
    if 'MYDATA' in os.environ:
        # Assume we're debugging here.
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)-15s %(levelname)8s %(name)s %(message)s')

    bc = BenchmarkClient(mydata_url)

    # Make sure we have a token; can start the browser to get one.
    token = bc.load_auth_token()
    if not token:
        raise exceptions.TokenTimeoutError()

    result = bc.submit_benchmark(benchmark_data)
    print(result)

    # If we get a location from the MyData server, show it in a browser.
    if result.location:
        with G.progress_lock:
            G.results_url = result.location
        import webbrowser
        webbrowser.open_new_tab(result.location)
