"""Timeouts for HTTP traffic, all in seconds."""

submit = 30
verify = 10

# This one is tricky, as the user may need to take the time to register a new
# Blender ID (which includes confirmation of their email address). We should
# not show too scary messages when it comes to timeout errors when waiting for
# a token, and just expect it to time out when a new Blender ID account is
# registered.
#
# It should be long enough for a normal flow, though, as after this timeout
# the temp HTTP server on localhost:$RANDOM is down again.
wait_for_token = 15
