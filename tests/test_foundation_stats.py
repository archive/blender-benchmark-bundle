import pathlib
import unittest

from benchmark.foundation import stats


class StatsTest(unittest.TestCase):
    def test_real_logs(self):
        fake_log = pathlib.Path(__file__).with_name('classroom.log')
        stat = stats.Stats()
        with fake_log.open('r') as logfile:
            for line in logfile:
                stat.update(line)

        self.assertEqual(25.91, stat.device_memory_usage)
        self.assertEqual(49.71, stat.device_peak_memory)

        # Rendering is done
        self.assertIsNone(stat.current_sample)
        self.assertIsNone(stat.total_samples)

        self.assertEqual(144, stat.current_tiles)
        self.assertEqual('Path Tracing Tile', stat.path_tracing)
        self.assertEqual(0.21, stat.pipeline_render_time)
        self.assertEqual(0.031718, stat.render_time_no_sync)
        self.assertEqual(0.182569, stat.total_render_time)
        self.assertEqual(144, stat.total_tiles)
