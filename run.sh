#!/bin/bash

set -e

UNAME=`which uname`

# Self versioning info
BOOTSTRAP_VERSION="0.1"

# Variables which are automatically detected based on particular blender distro
# we are benchmarking.
BLENDER_VERSION=""
PYTHON_VERSION=""
PYTHON_ABI=""

Black=''
Blue=''
Green=''
Cyan=''
Red=''
Purple=''
Brown=''
LGray=''
DGray=''
LBlue=''
LGreen=''
LCyan=''
LRed=''
LPurple=''
Yellow=''
White=''
Normal=''

# Check if stdout is a terminal.
if test -t 1; then
  # See if it supports colors.
  ncolors=$(tput colors)
  if test -n "$ncolors" && test $ncolors -ge 8; then
    Black='\033[00;30m'
    Blue='\033[00;34m'
    Green='\033[00;32m'
    Cyan='\033[00;36m'
    Red='\033[00;31m'
    Purple='\033[00;35m'
    Brown='\033[00;33m'
    LGray='\033[00;37m'
    DGray='\033[01;30m'
    LBlue='\033[01;34m'
    LGreen='\033[01;32m'
    LCyan='\033[01;36m'
    LRed='\033[01;31m'
    LPurple='\033[01;35m'
    Yellow='\033[01;33m'
    White='\033[01;37m'
    Normal='\033[00m'
  fi
fi

STRIP_SENSITIVE() {
  echo "${@/$SCRIPTPATH/<SCRIPT_PATH>}"
}

PRINT_HEADER() {
  echo -e "${LBlue}`STRIP_SENSITIVE "${@}"`${Normal}"
}

PRINT_ERROR() {
  echo -e "${LRed}Error: `STRIP_SENSITIVE "${@}"`${Normal}"
}

FATAL_ERROR() {
  PRINT_ERROR "${@}"
  exit 1
}

PRINT_INFO() {
  echo -e `STRIP_SENSITIVE "${@}"`
}

PRINT_HEADER "Cycles Benchmark Suite bootstrap v${BOOTSTRAP_VERSION}"

# Check uname exists.
# Without this we would not be able to know which Blender to run.
if [ -z "${UNAME}" ]; then
  PRINT_ERROR "Unable to find uname command."
  exit 1
fi

# Check whether we support operation system.
KERNEL_NAME=`$UNAME -s`
PRINT_INFO "Detected OS: ${White}${KERNEL_NAME}${Normal}"

case "${KERNEL_NAME}" in
  Darwin)
    SCRIPTPATH=`cd "$(dirname "${0}")"; pwd`
    BLENDER_PLATFORM="macos"
    # TODO(sergey): We assume only newer 64 bit MacOS machines.
    BINESS="64"
    ;;

  Linux)
    SCRIPT=$(readlink -f $0)
    SCRIPTPATH=`dirname $SCRIPT`
    BLENDER_PLATFORM="linux"
    MACHINE_TYPE=`uname -m`
    # TODO(sergey): Handle other architectures than x86_64/i686 here.
    if [ ${MACHINE_TYPE} == 'x86_64' ]; then
      BITNESS="64"
    else
      BITNESS="32"
    fi
    ;;

  CYGWIN*|MINGW32*|MSYS*)
    # TODO(sergey): Maybe support Cygwin in the future.
    FATAL_ERROR "On Windows platform run.bat is to be used."
    ;;

  *)
    FATAL_ERROR "Unknown OS, can not continue."
    ;;
esac

if [ -z "${BLENDER_DIR}" ]; then
    BLENDER_DIR="${SCRIPTPATH}/blender/${BLENDER_PLATFORM}${BITNESS}"
fi
BLENDER_BIN="${BLENDER_DIR}/blender"
SCENES_DIR="${SCRIPTPATH}/scenes"
BENCHMARK="${SCRIPTPATH}/benchmark/main.py"
CONFIGURE_SCRIPT="${SCRIPTPATH}/benchmark/configure.py"

# Detect Blender version.
if [ ! -f "${BLENDER_BIN}" ]; then
  FATAL_ERROR "Unable to find Blender executable."
fi
BLENDER_VERSION=`${BLENDER_BIN} --factory-startup --version 2>/dev/null |
                     head -n 1 | sed -r 's/Blender ([0-9]\.[0-9]{2}).*/\1/'`
PRINT_INFO "Detected Blender version: ${White}${BLENDER_VERSION}${Normal}"

# Detect Python version used by Blender.
PYTHON_VERSION=`${BLENDER_BIN} --factory-startup -b --python-expr \
            'import platform; print("Python Version: {}" . \
                format(platform.python_version()))' 2>/dev/null |
        grep "Python Version" |
        sed -r 's/.*\s([0-9]+)\.([0-9]+).*/\1.\2/'`
PRINT_INFO "Detected Python version: ${White}${PYTHON_VERSION}${Normal}"

# Detect Python ABI
PYTHON="${BLENDER_DIR}/${BLENDER_VERSION}/python/bin/python${PYTHON_VERSION}"
if [ ! -f "${PYTHON}" ]; then
  for ABI in m d md; do
    PYTHON_ABI="${ABI}"
    PYTHON="${BLENDER_DIR}/${BLENDER_VERSION}/python/bin/python${PYTHON_VERSION}${PYTHON_ABI}"
    if [ -f "${PYTHON}" ]; then
      break
    fi
  done
fi
PRINT_INFO "Detected Python ABI: ${White}${PYTHON_ABI}${Normal}"

PRINT_INFO "Using Python from ${White}${PYTHON}${Normal}"
PRINT_INFO "Running benchmark script ${White}${BENCHMARK}${Normal}"

"${PYTHON}" "${BENCHMARK}" \
    --blender "${BLENDER_BIN}" \
    --scenes-dir "${SCENES_DIR}" \
    --configure-script "${CONFIGURE_SCRIPT}" \
    "${@}"
