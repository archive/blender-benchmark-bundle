@echo off

rem set SCRIPTPATH_SLASH=%~dp0
rem set SCRIPTPATH=%SCRIPTPATH_SLASH:~0,-1%
set SCRIPTPATH=
set BOOTSTRAP_VERSION=0.1
set BLENDER_VERSION=

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set BITNESS=32 || set BITNESS=64

echo Cycles Benchmark Suite bootstrap v%BOOTSTRAP_VERSION%
echo Detected OS: Windows %BITNESS%bit

for /l %%x in (9, -1, 0) do (
  for /l %%y in (99, -1, 10) do (
    if EXIST %SCRIPTPATH%blender\windows%BITNESS%\%%x.%%y (
      set BLENDER_VERSION=%%x.%%y
      goto GOT_VERSION
    )
  )
)

:GOT_VERSION

set BLENDER_DIR=%SCRIPTPATH%blender\windows%BITNESS%
set BLENDER_BIN=%BLENDER_DIR%\blender.exe
set SCENES_DIR=%SCRIPTPATH%scenes
set PYTHON=%BLENDER_DIR%\%BLENDER_VERSION%\python\bin\python.exe
set BENCHMARK=%SCRIPTPATH%benchmark\main.py
set CONFIGURE_SCRIPT=%SCRIPTPATH%benchmark\configure.py


echo %BLENDER_DIR%
echo Using Python from %PYTHON%
echo Running benchmark script %BENCHMARK%

%PYTHON% %BENCHMARK% --blender "%BLENDER_BIN%" --scenes-dir "%SCENES_DIR%" --configure-script "%CONFIGURE_SCRIPT%" %*
