@echo off

SET ARCH=%1
set PATH=%PATH%;C:\Program Files\7-Zip\

if "%ARCH%" == "x64" (echo Building for 64bit) else (
  if "%ARCH%" == "x86" (echo Building for 32bit) else (
    echo Unknown architecture, specity x86 or x64
    exit /B 1
  )
)

rem %HOMEDRIVE%
rem cd %HOMEPATH%
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" %ARCH%
echo Use full exe names when running under bash, e.g. "msbuild.exe"
echo Loading bash, you may now use git and msbuild in the same console \o/.
"C:\Program Files\Git\bin\sh.exe" bundle.sh
